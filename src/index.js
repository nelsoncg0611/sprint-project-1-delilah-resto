require('dotenv').config();     //requerir .env para asignar puerto.
const express = require('express');     //requerir libreria express.
const app = express();
const swaggerOptions = require("./utils/swaggerOptions")
const PORT = process.env.PORT || 3020;
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use("/swagger", swaggerUI.serve, swaggerUI.setup(swaggerSpecs))

const rutasUsuarios = require("./route/usuarios.route");
const rutasProductos = require("./route/productos.route");
const rutasPedidos = require("./route/pedidos.route");
const rutasPagos = require("./route/metodosdepago.route");

app.use('/metodopagos', rutasPagos);
app.use('/usuarios', rutasUsuarios);
app.use('/productos', rutasProductos);
app.use('/pedidos', rutasPedidos);

app.listen(PORT, () => { console.log("index iniciado en el puerto: " + PORT); });