const express = require('express');
const router = express.Router();
const {obtenerMediosPago, aggMetodo, editarmetodo, eliminarMetodo } = require("../models/metodospago.models");
const middlewaresLogin = require("../middlewares/autenticacion.middleware");
const { esAdmin } = require("../middlewares/esAdmin.middleware");

/**
 * @swagger
 * /metodopagos/metodosdepago:
 *  get:
 *      summary: Obtener todos los metodos de pago
 *      tags: [METODOS DE PAGO]
 *      schema:
 *      responses:
 *          200:
 *              description: Metodos de pago disponibles
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/obtenermetodospago'
 */
router.get("/metodosdepago", middlewaresLogin,esAdmin ,(req, res) => {
    res.json(obtenerMediosPago());
})

/**
 * @swagger
 * /metodopagos/agremetodopago:
 *  post:
 *      summary: Ingresar un nuevo metodo de pago al sistema
 *      description: Ingresar un nuevo metodo de pago unico e irrepetible al sistema
 *      tags: [METODOS DE PAGO]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/newmethod'
 *      responses:
 *          201:
 *              description: Metodo de pago agregado
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: Metodo de pago agregado
 *          200:
 *              description: Si el metodo actualmente existe.
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: Este metodo de pago ya existe.
 */
router.post("/agremetodopago", esAdmin, middlewaresLogin,(req, res) => {
    const medio = req.body;
    res.json(aggMetodo(medio));
});

/**
 * @swagger
 * /metodopagos/editarmetodo/{IdMetodoDePago}:
 *  put:
 *      summary: Edita el nombre de los metodos de pago dispobibles.
 *      description: Edita el nombre de los metodos de pago indicado por medio de su ID.
 *      tags: [METODOS DE PAGO]
 *      parameters:
 *        - in: path
 *          name: IdMetodoDePago
 *          required: true
 *          schema:
 *              type: number
 *              example: 1
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/editmethod'
 *      responses:
 *          201:
 *              description: Se hallo y se actualizao correctamente el metodo de pago
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: Metodo de pago actualizado
 *          400:
 *              description: el ID indicado del metodo de pago no existe.
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: El ID indicado no existe en nuestros metodos de pago.
 * 
 */
router.put("/editarmetodo/:id",esAdmin, middlewaresLogin, (req, res) => {
    const {id} = req.params;
    const { metodo } = req.body;
    res.json(editarmetodo(id, metodo));
})

/**
 * @swagger
 * /metodopagos/eliminarmetodo:
 *  delete:
 *      summary: eliminar un metodo de pago del sistema
 *      description: Eliminar metodo de pago de nuestro sistema
 *      tags: [METODOS DE PAGO]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/deletemethod'
 *      responses:
 *          200:
 *              description: Metodo de pago hallado y eliminado
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: Metodo de pago eliminado correctamente.
 *          400:
 *              description: Si el ID indicado del metodo de pago no existe.
 *              content:
 *                  text/plain:
 *                      schema:
 *                          type: string
 *                          example: El ID indicado no existe en nuestros metodos de pago.
 * 
 */
router.delete("/eliminarmetodo",esAdmin, middlewaresLogin, (req, res) => {
    const {id} = req.body;
    res.json(eliminarMetodo(id));
})

/**
 * @swagger
 * tags:
 *  name: METODOS DE PAGO
 *  description: Seccion dedicada a los "METODOS DE PAGO"
 * 
 * components:
 *  schemas:
 *      obtenermetodospago:
 *          type: object
 *          properties:
 *              medio:
 *                  type: string
 *                  description: Nombre del metodo de pago
 *              id:
 *                  type: number
 *                  description: ID unico de nuestro metod de pago
 *          example:
 *              medio: Efectivo
 *              id: 1
 *      newmethod:
 *          type: object
 *          require:
 *              - medio
 *          properties:
 *              medio:
 *                  type: string
 *                  description: Nombre del nuevo metodo de pago a agregar.
 *          example:
 *              medio: "PayPal"
 *          
 * 
 *      deletemethod:
 *          type: object
 *          require:
 *              - id
 *          properties:
 *              id:
 *                  type: number
 *                  description: Id unico del metodo de pago que se desea eliminar.
 *          example:
 *              id: 1
 * 
 *      editmethod:
 *          type: object
 *          require:
 *              - metodo
 *          properties:
 *              metodo:
 *                  type: string
 *                  description: Nuevo nombre que se le asignara al metodo de pago indicado
 *          example:
 *              metodo: Nequi
 *          
 * 
 */
module.exports = router;