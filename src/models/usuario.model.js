const usuarios = 
[
    {
        "email": "soyadmin@gmail.com",
        "username": "administrador",
        "password": "12345",
        "isAdmin": true,
        "telefono": 3125567282,
        "direccion": "calle 20 #15-sur",
        "id": Date.now()
    },
    {
        "email": "soyroot@gmail.com",
        "username": "root",
        "password": "root",
        "isAdmin": true,
        "telefono": 3125567282,
        "direccion": "calle 20 #15-sur",
        "id": Date.now()+1  
    },
    {
        "email": "correo1@gmail.com",
        "username": "usuario1",
        "password": "12345",
        "isAdmin": false,
        "telefono": 3125567282,
        "direccion": "calle 20 #15-sur",
        "id": 1626319469087
    }
]

const obtenerUser = () => {
return usuarios;
};


const aggUser = (usuarioNuevo) => {
    usuarioNuevo.isAdmin = false;
    usuarioNuevo.id = Date.now();
    const validar = usuarios.some(valor => {
        return valor.email == usuarioNuevo.email;
    })
    if (validar == true) {
        return true;
    } else {usuarios.push(usuarioNuevo);}
}


const ingresar = (email, password) => {
    const validacion = usuarios.some(e => {
        return e.password == password && e.email == email;
    });
    return validacion;
};

module.exports = { obtenerUser, aggUser, ingresar };