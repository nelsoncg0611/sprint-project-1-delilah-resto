const { obtenerUser } = require("../models/usuario.model")
const { tipoPago } = require("../models/metodospago.models")
const { obtenerProducto } = require("./producto.model");
const { json } = require("express");
const variable = [
    {
        "usuario": "usuario1",
        "id_usuario": 1626319469087,
        "id_pedido": 999,
        "orden": [
            {
                "nombre": "Botella CocaCola ZERO",
                "id": 96,
                "precio": 1500,
                "cantidad": 1
            },
            {
                "nombre": "Hamburguesa con pollo y mayonesa",
                "id": 200,
                "precio": 8200,
                "cantidad": 2
            }
        ],
        "precio_total": 9200,
        "direccion_Pedido": "carrera 10 #16-12",
        "estado_Pedido": "Cerrado",
        "metodo_pago": {
            "medio": "Efectivo",
            "id": 1
        }
    }
];

const hacerPedidos = (username, idProductosOrden, idDepago, direccionDePedido) => { //Realizar pedido de usuario logueado.
    let mensajeRespuesta = "";
    const productosOrden = [];
    let precioTotal = 0;
    const userPedExistente = obtenerUser().find(user => username === user.email);
    const existePedido = variable.filter(pedido => pedido.id_usuario == userPedExistente.id);
    const estadoCerrado = existePedido.every(pedido => pedido.estado_Pedido == "Cerrado");
    if (estadoCerrado != true) {
        return mensajeRespuesta = "Ya tiene un pedido en proceso"
    } else {
    
        const copiaProductos = JSON.parse(JSON.stringify(obtenerProducto()));
    idProductosOrden.forEach(elemento => {
        const Idproducto = copiaProductos.find(productos => elemento.id == productos.id);
        if(Idproducto != undefined && elemento.id == Idproducto.id){
            productosOrden.push(Idproducto);
                precioTotal += Idproducto.precio * elemento.cantidad;
                Idproducto.cantidad = elemento.cantidad;
        } else {mensajeRespuesta += `El ID ${elemento.id} no existe en nuestros productos `}
    });
    if (mensajeRespuesta != ""){return mensajeRespuesta} else {
    const validar = obtenerUser().find(user => username === user.email);
    if (tipoPago(idDepago) == undefined) {return mensajeRespuesta = "El ID de pago es invalido"} else {
    const orden = {
        "usuario": validar.username,
        "id_usuario": validar.id,
        "id_pedido": Math.floor((Math.random() * 100) + 500),
        "orden": productosOrden,
        "precio_total": precioTotal,
        "direccion_Pedido": direccionDePedido,
        "estado_Pedido": "Pendiente",
        "metodo_pago": tipoPago(idDepago)
    };
    mensajeRespuesta = `Pedido numero ${orden.id_pedido} Creado exitosamente`
    variable.push(orden);
    }}};
    return mensajeRespuesta
};

const obtenerMiPedido = (usuarioAuth) => { //Obtener el pedido de usuario logueado.
    let misPedidos = [];
    variable.forEach(elemento => {
        const usuario = obtenerUser().find(user => user.email === usuarioAuth);
        if (usuario.id === elemento.id_usuario) {
            misPedidos.push(elemento);
        }
    });
    return misPedidos;
}

const todoPedidos = () => { //Obtener los pedidos de todos los usuarios (solo admin).
    return variable
};

const cambiarEstado = (id, estado) => {
    const estadoId = variable.find(valor => valor.id_pedido == id);
    if(estadoId == undefined) {return `No hallamos pedido con el ID indicado`}
    estadoId.estado_Pedido = estado;
    return `El pedido identificado con ID ${id} paso a estado ${estado}`
}

const cambiarEstadoUser = (id) => {
    const estadoId = variable.find(valor => valor.id_pedido == id)
    if(estadoId == undefined) {return "El ID del pedido indicado no existe"
        } else if (estadoId.estado_Pedido == "Pendiente") {
            estadoId.estado_Pedido = "Confirmado";
            return "El estado del pedido paso a Confirmado"
            }       else {return "Este pedido ya esta confirmado"}
};


const editarPedido = (id, pedidos) => { //cambiar cantidad del producto *se cambia tambien precio total*
    let mensajeRespuesta = "No hallamos el ID del producto a modificar";
    variable.forEach(hallarPedido => { //recorremos el array de pedidos ya enviados
        if (id == hallarPedido.id_pedido) { //verificamos que el id que envian por params si existe
             if (hallarPedido.estado_Pedido != "Pendiente") {return mensajeRespuesta = "Su pedido esta en estado confirmado por ende no se puede modificar"} else{  
              obtenerProducto().forEach(prod => { //recorremos el array de productos
                    if (pedidos.id == prod.id) {
                        hallarPedido.orden.forEach(miOrden => {
                            if(pedidos.id == miOrden.id){                            
                            hallarPedido.precio_total -= miOrden.cantidad * prod.precio;
                            miOrden.cantidad = pedidos.cantidad;
                            hallarPedido.precio_total += pedidos.cantidad * prod.precio;
                            return mensajeRespuesta = "Cantidad modificada exitosamente"
                        }
                        })
                    }
             })};

        } else {
            return  mensajeRespuesta = "No hallamos el ID de pedido indicado"
        }
    });
    return mensajeRespuesta
}

const eliminarProductoDePedido = (idPedido, idproducto) => {
    let mensajeRespuesta = "";
    const pedidoIndicado = variable.find(pedido => pedido.id_pedido == idPedido);
    if (pedidoIndicado == undefined) {
        return mensajeRespuesta = "El ID de pedido indicado no es correcto o no existe."
    }   else {
        if (pedidoIndicado.estado_Pedido != "Pendiente") {return mensajeRespuesta = "Su pedido esta en estado confirmado por ende no se puede modificar"} else{
        pedidoIndicado.orden.forEach((elemento, index) => {
            if(elemento.id == idproducto){
                pedidoIndicado.orden.splice(index, 1);
                return mensajeRespuesta = `producto ${elemento.nombre} eliminado correctamente del pedido`
            } else { return mensajeRespuesta = "El ID del producto a eliminar es invalido o no existe"}
        })}
        pedidoIndicado.precio_total = 0;
        pedidoIndicado.orden.forEach((elemento => {
            pedidoIndicado.precio_total += elemento.cantidad * elemento.precio
        }))

    }
    return mensajeRespuesta
}
    

const agregarProductosPedido = (idMiPedido, pedidoCompleto) => {
    mensajeRespuesta = "";
    variable.forEach(pedido => {
        if (pedido.id_pedido == idMiPedido) {
            if(pedido.estado_Pedido != "Pendiente"){return mensajeRespuesta = "Su pedido esta en estado confirmado por ende no se puede modificar"}else{
            const copiaProductos = JSON.parse(JSON.stringify(obtenerProducto()));
            const productoNuevo = copiaProductos.find(prod => prod.id == pedidoCompleto.id); //Aqui se halla el producto solicitado desde params
            if(productoNuevo == undefined){ return mensajeRespuesta = "El producto indicado por ID no existe"}
            const productoExiste = pedido.orden.some(idProd => idProd.id == productoNuevo.id); //Aqui se devuelve si ese pedido existe o no en nuestra orden
                if (productoExiste == true ) {      //Si existe vamos a recorrer la orden y sumar a la cantidad actual del pedido la cantidad que viene de params
                    pedido.orden.forEach(elemento => {
                    if(elemento.id == productoNuevo.id){
                        precioAnterior = pedido.precio_total;
                        elemento.cantidad += pedidoCompleto.cantidad
                        precioParcial = pedidoCompleto.cantidad * productoNuevo.precio;
                        pedido.precio_total = precioParcial + precioAnterior;
                    }
                })
                    return mensajeRespuesta = "Se agrego la cantidad deseada a tu producto ya que ya existe en tu pedido"
                } else {
                    productoNuevo.cantidad = pedidoCompleto.cantidad;
                    pedido.orden.push(productoNuevo)
                    pedido.precio_total += pedidoCompleto.cantidad * productoNuevo.precio;
                    return mensajeRespuesta = `Se añadio correctamente el producto a tu pedido`;}
    }}
    else {} 
        return mensajeRespuesta = "El ID de pedido indicado no es correcto"  
    
    })
    return mensajeRespuesta;
};

module.exports = { hacerPedidos, todoPedidos, obtenerMiPedido, cambiarEstado, cambiarEstadoUser, editarPedido, eliminarProductoDePedido, agregarProductosPedido };