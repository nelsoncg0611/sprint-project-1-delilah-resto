const { esNumero, esString } = require ("../middlewares/modificarpedido.middleware")
let productos = [
    {
        "nombre": "Botella CocaCola ZERO",
        "id": 96,
        "precio": 1500,    
    },
    {
        "nombre": "Hamburguesa con pollo y mayonesa",
        "id": 200,
        "precio": 8200
    },
    {
        "nombre": "Pizza de Carnes",
        "id": Math.floor((Math.random()*100)+50),
        "precio": 5000
    },
    {
        "nombre": "Cerezada",
        "id": Math.floor((Math.random()*100)+50),
        "precio": 10000
    }
]

//Obtener los productos
const obtenerProducto = () => {
    return productos;
};

//Editar un producto por medio de un id (id pasado por .params)
const editarProducto = (idParametro, productosActualizar) => {
    const resultado = productos.find(valor =>idParametro == valor.id );
    if(resultado === undefined) {return "El ID del producto a modificar no existe"};
        const validarPrecio = esNumero(productosActualizar.precio);
        const validarProducto = esString(productosActualizar.nombre);
            if(validarPrecio === true && validarProducto === true){
                resultado.precio = productosActualizar.precio;
                resultado.nombre = productosActualizar.nombre;
        } else {return "El precio del producto debe ser un numero, no una palabra o string y el nombre del producto debe ser una palabra y/ o string"}
    return "Producto actualizado correctamente"
};

//Eliminar un producto por medio de un id (id pasado por .params)
const eliminarProducto = (idParametro) => {
    let mensajeRespuesta = "";
    const productoRequerido = productos.find(valor => valor.id == idParametro);
    const indexDeProducto = productos.findIndex(valor => valor.id == idParametro);
        if (productoRequerido != undefined){
            productos.splice(indexDeProducto, 1);
            return `El producto ${productoRequerido.nombre} fue eliminado correctamente`;
        } else {return `El ID indicado no corresponde a ningun producto`;};
};

//Dar de alta nuevos productos
const agregarProducto = (productoNuevo) => {
    mensajeRespuesta = ""
    const productoExiste = productos.some(prod => prod.nombre == productoNuevo.nombre)
    if (productoExiste === true) return "El producto ya existe"
    if(productoNuevo.nombre === undefined || productoNuevo.precio === undefined){ 
        return mensajeRespuesta = "Las caracteristicas de precio y nombre del producto deben estar definidas";
    }
    const validarPrecio = esNumero(productoNuevo.precio);
    const validarNombre = esString(productoNuevo.nombre);
    if (validarPrecio === false || validarNombre === false) {
        return mensajeRespuesta = "El nombre del producto debe ser un string y el precio un numero"
    }
    productos.push(productoNuevo)
    productoNuevo.id = Math.floor((Math.random()*100)+50)
    return "Producto agregado exitosamente"
};
module.exports = { obtenerProducto, editarProducto, eliminarProducto, agregarProducto }