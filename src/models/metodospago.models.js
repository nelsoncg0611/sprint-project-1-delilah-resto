const metodosDePago = [
    {
        "medio": "Efectivo",
        "id": 1
    },
    {
        "medio": "Tarjeta debito",
        "id": Math.floor((Math.random() * 100) + 10)
    },
    {
        "medio": "Tarjeta credito",
        "id": Math.floor((Math.random() * 100) + 10)
    }
];

const obtenerMediosPago = () => {
    return metodosDePago;
}

const aggMetodo = (metodoPago) => {
    metodoPago.id = Math.floor((Math.random() * 100) + 10)
    const validacion = metodosDePago.some(elemento => elemento.medio == metodoPago.medio);
    if (validacion == true) {
        return "Este metodo de pago ya existe"
    } else {    
    metodosDePago.push(metodoPago);
    return "Metodo de pago agregado"};
};

const editarmetodo = (idMetodo, cambioMetodo) => {
    if ( metodosDePago.some(elemento => elemento.id == idMetodo)){
        if(cambioMetodo != undefined){
            const miMetodo = metodosDePago.find(elemento => elemento.id == idMetodo);
            miMetodo.medio = cambioMetodo
            return "metodo de pago actualizado"
        } else {return "Por favor defina un metodo de pago"}
    } else {return "El id indicado no existe"}
};

const eliminarMetodo = (idMetodo) => {
    if ( metodosDePago.some(elemento => elemento.id == idMetodo)){
        const IndexMetodo = metodosDePago.findIndex(elemento => elemento.id == idMetodo);
        metodosDePago.splice(IndexMetodo, 1);
        return "Metodo de pago eliminado correctamente"    
    } else {return "El id indicado no existe"}
}

const tipoPago = (id) => {
    return metodosDePago.find(metodo => id === metodo.id);
 }
module.exports = { tipoPago, obtenerMediosPago, aggMetodo, editarmetodo, eliminarMetodo }
